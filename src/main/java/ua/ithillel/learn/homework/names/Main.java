package ua.ithillel.learn.homework.names;

public class Main {
    public static void main(String[] args) {

        Human humanOne = new Human("Peter", "Parker");
        Human humanTwo = new Human("Bruce", "Wayne", "Thomasovich");

        System.out.println(humanOne.getFullName());
        System.out.println(humanTwo.getFullName());

        System.out.println(humanOne.getShortName());
        System.out.println(humanTwo.getShortName());

    }
}
