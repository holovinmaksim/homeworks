package ua.ithillel.learn.homework.names;

import java.util.Objects;

public class Human {

    String name;
    String surname;
    String patronymic;

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Human(String name, String surname, String patronymic) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    public String getFullName() {
        if (patronymic != null) {
            return surname + " " + name + " " + patronymic;
        } else {
            return surname + " " + name;
        }
    }


    public String getShortName() {
        if (patronymic != null) {
            return surname + " " + name.charAt(0) + "." + patronymic.charAt(0) + ".";
        } else {
            return surname + " " + name.charAt(0) + ".";
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(patronymic, human.patronymic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, patronymic);
    }

    @Override
    public String toString() {
        return "Name: " + name + '\'' +
                "Surname: " + surname + '\'' +
                "Patronymic: " + patronymic;

    }
}

