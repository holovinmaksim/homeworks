package ua.ithillel.learn.homework.dots;

import ua.ithillel.learn.homework.dots.menu.Menu;
import ua.ithillel.learn.homework.dots.menu.MenuItem;
import ua.ithillel.learn.homework.dots.services.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        UserInterface userInterface = new UserInterface(scanner);

        Circle circle = new Circle(new Point(scanner), scanner, userInterface);
        circle.setRadius(0);
        Point point = new Point(0,0);
        circle.setCenter(point);
        PointList list = new PointList(scanner, userInterface);

        Menu menu = new Menu(scanner, new MenuItem[]{

                new CreatePointListMenuItem(list),
                new CreateCircleMenuItem(circle),
                new CreatePointMenuItem(list),
                new ShowAllPoints(list, userInterface),
                new ShowPointsInCircleMenuItem(circle, list),
                new ShowPointsOutCircleMenuItem(circle, list),
                new ExitMenu()
        }
        );
        menu.run();
    }
}

