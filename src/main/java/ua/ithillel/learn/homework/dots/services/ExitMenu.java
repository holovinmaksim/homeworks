package ua.ithillel.learn.homework.dots.services;

import ua.ithillel.learn.homework.dots.menu.MenuItem;

public class ExitMenu implements MenuItem {
    @Override
    public String getName() {
        return "Exit.";
    }

    @Override
    public void execute() {
        System.out.println("Thank you!");
    }

    @Override
    public boolean ifFinal() {
        return true;
    }
}

