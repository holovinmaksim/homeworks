package ua.ithillel.learn.homework.dots.services;

import ua.ithillel.learn.homework.dots.Circle;
import ua.ithillel.learn.homework.dots.menu.MenuItem;

public class CreateCircleMenuItem implements MenuItem {
    private Circle circle;

    public CreateCircleMenuItem(Circle circle) {
        this.circle = circle;
    }

    @Override
    public String getName() {
        return "Change circle. ";
    }

    @Override
    public void execute() {
        circle.createCircle();
    }
}
