package ua.ithillel.learn.homework.dots.services;

import ua.ithillel.learn.homework.dots.PointList;
import ua.ithillel.learn.homework.dots.UserInterface;
import ua.ithillel.learn.homework.dots.menu.MenuItem;

public class ShowAllPoints implements MenuItem {
    private PointList pointList;
    private UserInterface userInterface;

    public ShowAllPoints(PointList pointList, UserInterface userInterface) {
        this.pointList = pointList;
        this.userInterface = userInterface;
    }

    @Override
    public String getName() {
        return "Show all points. ";
    }

    @Override
    public void execute() {
        userInterface.showPointList(pointList);
    }
}
