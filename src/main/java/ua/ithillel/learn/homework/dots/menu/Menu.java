package ua.ithillel.learn.homework.dots.menu;

import java.util.Scanner;

public class Menu {
    MenuItem[] items;
    Scanner scanner;

    public Menu(Scanner scanner, MenuItem[] items) {
        this.scanner = scanner;
        this.items = items;
    }

    public void run() {
        while (true) {
            showMenu();
            int choice = getUserChoice();
            if (choice < 0 || choice >= items.length) {
                System.out.println("Incorrect choice, please enter from this list: ");
                continue;
            }
            items[choice].execute();
            if (items[choice].ifFinal()) {
                break;
            }
        }
    }

    private void showMenu() {
        System.out.println("_________________________________________________________________");
        for (int i = 0; i < items.length; i++) {
            System.out.printf("%2d. %s\n", (i + 1), items[i].getName());
        }
        System.out.println("_________________________________________________________________");
        System.out.println();

    }

    private int getUserChoice() {
        System.out.println("Enter your choice from this list: ");
        int choice = scanner.nextInt();
        scanner.nextLine();
        return choice - 1;
    }


}


