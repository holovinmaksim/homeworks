package ua.ithillel.learn.homework.dots;

import java.util.Scanner;

public class PointList {
    private Point pointList[];
    private int listSize;
    private Scanner scanner;
    private UserInterface userInterface;

    public PointList(Scanner scanner, UserInterface userInterface) {
        this.scanner = scanner;
        this.userInterface = userInterface;
    }

    public int getListSize() {
        return listSize;
    }

    public Point getPointFromList(int counter){
        return pointList[counter];
    }

    public void initPointList(){
        System.out.println("Enter how many points you want to add: ");
        listSize = scanner.nextInt();
        scanner.nextLine();
        pointList = new Point[listSize];
        System.out.println();
    }

    public void fillMassWithPoints(){
        for(int i = 0; i < pointList.length; i++){
            pointList[i] = new Point(scanner);
            pointList[i] = userInterface.getUserPoint(new Point(scanner));
            if(i != pointList.length - 1 && userInterface.userChoice()){
                continue;
            }else{
                break;
            }
        }
    }

    public void addPointToList(){
        pointList = copyOf(pointList, listSize);
        pointList[listSize] = userInterface.getUserPoint(new Point(scanner));
        listSize++;
    }

    private Point[] copyOf(Point points[], int size){
        points = new Point[size + 1];
        for(int i = 0; i < pointList.length; i++){
            points[i] = pointList[i];
        }
        return points;
    }
}
