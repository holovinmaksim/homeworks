package ua.ithillel.learn.homework.dots.services;

import ua.ithillel.learn.homework.dots.Circle;
import ua.ithillel.learn.homework.dots.PointList;
import ua.ithillel.learn.homework.dots.menu.MenuItem;

public class ShowPointsOutCircleMenuItem implements MenuItem {

    private Circle circle;
    private PointList pointList;

    public ShowPointsOutCircleMenuItem(Circle circle, PointList pointList) {
        this.circle = circle;
        this.pointList = pointList;
    }

    @Override
    public String getName() {
        return "Show points outside the circle.";
    }

    @Override
    public void execute() {
        circle.getPointOutOfCircle(pointList);
    }
}
