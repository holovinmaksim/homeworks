package ua.ithillel.learn.homework.dots;


import java.util.Objects;
import java.util.Scanner;

public class Circle {


    public Point center;
    public double radius;
    private Scanner scanner;
    private UserInterface userInterface;

    public void setCenter(Point center) {
        this.center = center;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Circle(Point center, Scanner scanner, UserInterface userInterface) {
        this.center = center;
        this.scanner = scanner;
        this.userInterface = userInterface;
    }

    public void createCircle(){
        userInterface.getUserCircleCenter(center);
        radius = userInterface.getUserCircleRadius();
    }

    public void getPointInCircle(PointList pointList){
        System.out.println("This point in circle: ");
        for(int i = 0; i < pointList.getListSize(); i++){
            if(pointList.getPointFromList(i) != null) {
                double distance = center.getDistanceTo(pointList.getPointFromList(i));
                if (distance <= radius) {
                    userInterface.showPointsInCircle(pointList.getPointFromList(i));
                } else {
                    continue;
                }
            }
        }
    }

    public void getPointOutOfCircle(PointList pointList){
        System.out.println("This point out of circle: ");
        for(int i = 0; i < pointList.getListSize(); i++){
            if(pointList.getPointFromList(i) != null) {
                double distance = center.getDistanceTo(pointList.getPointFromList(i));
                if (distance >= radius) {
                    userInterface.showPointsOutCircle(pointList.getPointFromList(i));
                } else {
                    continue;
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Circle)) return false;
        Circle circle = (Circle) o;
        return Double.compare(circle.radius, radius) == 0 && Objects.equals(center, circle.center);
    }

    @Override
    public int hashCode() {
        return Objects.hash(center, radius);
    }

    @Override
    public String toString() {
        return  "Center: " + center + ", Radius: " + radius;
    }
}

