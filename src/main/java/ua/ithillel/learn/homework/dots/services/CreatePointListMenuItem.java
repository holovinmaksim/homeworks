package ua.ithillel.learn.homework.dots.services;

import ua.ithillel.learn.homework.dots.PointList;
import ua.ithillel.learn.homework.dots.menu.MenuItem;

public class CreatePointListMenuItem implements MenuItem {

    private PointList pointList;

    public CreatePointListMenuItem(PointList pointList) {
        this.pointList = pointList;
    }

    @Override
    public String getName() {
        return "Create point-list.";
    }

    @Override
    public void execute() {
        pointList.initPointList();
        pointList.fillMassWithPoints();
    }
}

