package ua.ithillel.learn.homework.dots.menu;

public interface MenuItem {

    String getName();

    void execute();

    default boolean ifFinal() {
        return false;
    }
}
