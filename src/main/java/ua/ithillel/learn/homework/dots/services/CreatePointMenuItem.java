package ua.ithillel.learn.homework.dots.services;

import ua.ithillel.learn.homework.dots.PointList;
import ua.ithillel.learn.homework.dots.menu.MenuItem;

public class CreatePointMenuItem implements MenuItem {

    private PointList pointList;

    public CreatePointMenuItem(PointList pointList) {
        this.pointList = pointList;
    }

    @Override
    public String getName() {
        return "Create point and add to list.";
    }

    @Override
    public void execute() {
        pointList.addPointToList();
    }
}
