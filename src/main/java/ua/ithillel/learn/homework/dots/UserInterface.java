package ua.ithillel.learn.homework.dots;

import java.util.Scanner;

public class UserInterface {
    private Scanner scanner;

    public UserInterface(Scanner scanner) {
        this.scanner = scanner;
    }

    public Point getUserPoint(Point point) {
        System.out.println("Enter coordinates of your point: ");

        System.out.print("Coordinates for X: ");
        point.coordinatesX = scanner.nextDouble();
        scanner.nextLine();

        System.out.print("Coordinates for Y: ");
        point.coordinatesY = scanner.nextDouble();
        scanner.nextLine();

        return point;
    }

    public void getUserCircleCenter(Point center) {
        System.out.println("Coordinates of center of circle: ");
        getUserPoint(center);
    }

    public double getUserCircleRadius() {
        System.out.println("Enter number for the radius of the circle: ");
        double radius = scanner.nextDouble();
        scanner.nextLine();
        return radius;
    }

    public void showPointsInCircle(Point point) {
        System.out.println("CoordinatesX: " + point.getCoordinatesX());
        System.out.println("CoordinatesY: " + point.getCoordinatesY());
    }

    public void showPointsOutCircle(Point point) {
        System.out.println("CoordinatesX: " + point.getCoordinatesX());
        System.out.println("CoordinatesY: " + point.getCoordinatesY());
    }

    public boolean userChoice() {
        int userChoice;
        System.out.println("Add another point? Enter '1' - for YES, enter '2' - for NO: ");
        userChoice = scanner.nextInt();
        scanner.nextLine();
        return userChoice == 1;
    }

    public void showPointList(PointList pointList){
        for (int i = 0; i < pointList.getListSize(); i++) {
            System.out.println("Point " + (i+1) + ":");
            System.out.println("Coordinates X: " + pointList.getPointFromList(i).getCoordinatesX());
            System.out.println("Coordinates Y: " + pointList.getPointFromList(i).getCoordinatesY());
        }
    }

}
