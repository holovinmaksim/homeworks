package ua.ithillel.learn.homework.dots.services;


import ua.ithillel.learn.homework.dots.Circle;
import ua.ithillel.learn.homework.dots.PointList;
import ua.ithillel.learn.homework.dots.menu.MenuItem;

public class ShowPointsInCircleMenuItem implements MenuItem {

    private Circle circle;
    private PointList pointList;

    public ShowPointsInCircleMenuItem(Circle circle, PointList pointList) {
        this.circle = circle;
        this.pointList = pointList;
    }

    @Override
    public String getName() {
        return "Show points inside the circle.";
    }

    @Override
    public void execute() {
        circle.getPointInCircle(pointList);
    }
}
