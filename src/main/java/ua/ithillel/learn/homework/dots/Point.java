package ua.ithillel.learn.homework.dots;

import java.util.Objects;
import java.util.Scanner;

import static java.lang.Math.pow;

public class Point {


    public double coordinatesX;
    public double coordinatesY;
    private Scanner scanner;


    public double getCoordinatesX() {
        return coordinatesX;
    }

    public double getCoordinatesY() {
        return coordinatesY;
    }

    public void setCoordinatesX(double coordinatesX) {
        this.coordinatesX = coordinatesX;
    }

    public void setCoordinatesY(double coordinatesY) {
        this.coordinatesY = coordinatesY;
    }
    public Point(double coordinatesX, double coordinatesY) {
        this.coordinatesX = coordinatesX;
        this.coordinatesY = coordinatesY;
    }

    public Point(Scanner scanner) {
        this.scanner = scanner;
    }

    public double getDistanceTo(Point point) {
        return Math.sqrt(pow((point.coordinatesX - this.coordinatesX), 2) + pow((point.coordinatesY - this.coordinatesY), 2));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point)) return false;
        Point point = (Point) o;
        return Double.compare(point.getCoordinatesX(), getCoordinatesX()) == 0 && Double.compare(point.getCoordinatesY(), getCoordinatesY()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCoordinatesX(), getCoordinatesY());
    }

    @Override
    public String toString() {
        return "Coordinates X: " + coordinatesX + ", Coordinates Y: " + coordinatesY;
    }
}


